import unittest
from src.main import app
from fastapi.testclient import TestClient


class TestApp(unittest.TestCase):

    def setUp(self):
        self.client = TestClient(app)

    def test_get_root(self):
        response = self.client.get("/")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"msg": "Hello World"})


if __name__ == '__main__':
    unittest.main()
