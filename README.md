[![pipeline status](https://gitlab.com/nicolalandro/fastapitest/badges/main/pipeline.svg)](https://gitlab.com/nicolalandro/fastapitest/-/commits/main)
[![coverage report](https://gitlab.com/nicolalandro/fastapitest/badges/main/coverage.svg)](https://gitlab.com/nicolalandro/fastapitest/-/commits/main)

[![coverage report](https://gitlab.com/nicolalandro/fastapitest/badges/main/coverage.svg?job=pytest&key_text=pytest+Coverage&key_width=130)](https://gitlab.com/nicolalandro/fastapitest/-/commits/main)
[![coverage report](https://gitlab.com/nicolalandro/fastapitest/badges/main/coverage.svg?job=unittest&key_text=unittest+Coverage&key_width=130)](https://gitlab.com/nicolalandro/fastapitest/-/commits/main)
[![coverage report](https://gitlab.com/nicolalandro/fastapitest/badges/main/coverage.svg?job=doctest&key_text=doctest+Coverage&key_width=130)](https://gitlab.com/nicolalandro/fastapitest/-/commits/main)
[![coverage report](https://gitlab.com/nicolalandro/fastapitest/badges/main/coverage.svg?job=mamba&key_text=mamba+Coverage&key_width=130)](https://gitlab.com/nicolalandro/fastapitest/-/commits/main)
[![coverage report](https://gitlab.com/nicolalandro/fastapitest/badges/main/coverage.svg?job=robotframework&key_text=robotframework+Coverage&key_width=160)](https://gitlab.com/nicolalandro/fastapitest/-/commits/main)

[comment]: <> (the build query do not work so you can have multiple coverages but not multiple builds)

[comment]: <> ([![pipeline status]&#40;https://gitlab.com/nicolalandro/fastapitest/badges/main/pipeline.svg?job=pytest&key_text=pytest+Build&key_width=130&#41;]&#40;https://gitlab.com/nicolalandro/fastapitest/-/commits/main&#41;)

[comment]: <> ([![pipeline status]&#40;https://gitlab.com/nicolalandro/fastapitest/badges/main/pipeline.svg?job=unittest&key_text=unittest+Build&key_width=130&#41;]&#40;https://gitlab.com/nicolalandro/fastapitest/-/commits/main&#41;)

[comment]: <> ([![pipeline status]&#40;https://gitlab.com/nicolalandro/fastapitest/badges/main/pipeline.svg?job=doctest&key_text=doctest+Build&key_width=130&#41;]&#40;https://gitlab.com/nicolalandro/fastapitest/-/commits/main&#41;)

[comment]: <> ([![pipeline status]&#40;https://gitlab.com/nicolalandro/fastapitest/badges/main/pipeline.svg?job=mamba&key_text=mamba+Build&key_width=130&#41;]&#40;https://gitlab.com/nicolalandro/fastapitest/-/commits/main&#41;)

[comment]: <> ([![pipeline status]&#40;https://gitlab.com/nicolalandro/fastapitest/badges/main/pipeline.svg?job=robotframework&key_text=robotframework+Build&key_width=160&#41;]&#40;https://gitlab.com/nicolalandro/fastapitest/-/commits/main&#41;)

[comment]: <> ([![pipeline status]&#40;https://gitlab.com/nicolalandro/fastapitest/badges/main/pipeline.svg?job=linting&key_text=flake8+Build&key_width=130&#41;]&#40;https://gitlab.com/nicolalandro/fastapitest/-/commits/main&#41;)

[comment]: <> ([![pipeline status]&#40;https://gitlab.com/nicolalandro/fastapitest/badges/main/pipeline.svg?job=code-vulnerabilities&key_text=bandit+Build&key_width=130&#41;]&#40;https://gitlab.com/nicolalandro/fastapitest/-/commits/main&#41;)

[comment]: <> ([![pipeline status]&#40;https://gitlab.com/nicolalandro/fastapitest/badges/main/pipeline.svg?job=libs-vulnerabilities&key_text=safety+Build&key_width=130&#41;]&#40;https://gitlab.com/nicolalandro/fastapitest/-/commits/main&#41;)


# Fast API how to test
This repo is a guide/study to create test for Fast API with any lib to find the better.

```
python3.8 -m venv venv

source venv/bin/activate # remember to activate the env eny times if you use it
# or for fish shell: source venv/bin/activate.fish

pip install -r requirements.txt
```

## Pytest
the tests are in folder pytest_folder and datarts with test_
```
python -m pytest -s --cov=src pytest_folder
# -s to see string print, --cov=folder_code to have the code coverage
```

## Coverage usage
Coverage is a python lib that can be used to compute code coverage after running a command.
Base use:
```
coverage run --source=src -m <A COMMAND>
```
Aftert that command we find a .coverage file with informations, we can get the report in the terminal with:
```
coverage report
```
It is also possible to have a web based version most readable with more information for each files with:
```
coverage html
firefox htmlcov/index.html
```

## Unittest
the tests are in folder unittest_folder and starts with test_
```
python -m unittest discover
# or with coverage
coverage run --source=src -m unittest discover
coverage report
```

## Doctest
the test are into comments inside src/main.py
```
python -m doctest -v src/main.py
# or with coverage
coverage run --source=src -m doctest -v src/main.py
coverage report
```


## mamba
the tests are in folder mamba_folder and ends with _spec
```
python -m mamba.cli mamba_folder
# or with coverage
coverage run --source=src -m mamba.cli mamba_folder
coverage report
```

## Robot framework
the tests are in folder robot_folder and ends with .robot
```
python -m robot robot_folder
# or with coverage
coverage run --source=src -m robot robot_folder
coverage report
```

# Other Quality checks
Typically, this tools are runned:

1. While coding (inside your IDE or code editor)
2. At commit time (with pre-commit hooks)
3. When code is checked in to source control (via a CI pipeline)


## flake8
I use flake8 and pep8-naming as linting tools.The linting underline the code smell using a static analisys. 
```
python -m flake8 src mamba_folder pytest_folder unittest_folder robot_folder
```

## Bandit
I use Bandit as Security Vulnerability Scanner, that scan your code for possible vulnerabilities like eval function that eval strings.
```
python -m bandit -r src mamba_folder unittest_folder robot_folder
# i remove pytest_folder because of assert instruction is a smell, it will be removed if you compile to optimised byte code.
```

## Safety
Safety is a python lib that chek the vulnerabilities of libraries that I use into my env.
```
python -m safety check
```

# References
* [fastapi](https://fastapi.tiangolo.com/tutorial/first-steps/): a python framework for creation of API faster
* [fastapi test (pytest)](https://fastapi.tiangolo.com/tutorial/testing/): how to test FastApi official guide (use pytest)
* [pytest](https://docs.pytest.org/en/latest/): function based python library for test automation
* [pytest-cov](https://pypi.org/project/pytest-cov/): test coverage automatic computation for pytest
* [unittest](https://docs.python.org/3/library/unittest.html): class based python library for test automation
* [coverage](https://pypi.org/project/coverage/): a python generic tool to compute code coverage for each type of python runnter
* [doctest](https://docs.python.org/3/library/doctest.html): documentation based python library for test automation
* [Better Specs](https://www.betterspecs.org/): collection of best practice for Spec test ideology
* [mamba](https://pypi.org/project/mamba/): Spec based python library for test automation
* [expects](https://pypi.org/project/expects/): Spec asserction library
* [robotframework](https://robotframework.org/): Human Readable based python library for test automation
* [flake8](https://pypi.org/project/flake8/): For linting check
* [pep8-naming](https://pypi.org/project/pep8-naming/): add naming python rule to flake8 check
* [bandit](https://pypi.org/project/bandit/): check the code possible vulnerabilities
* [safety](https://pypi.org/project/safety/): check from a db if the installed libs have vulnerabilities
* [Python Code Quality](https://testdriven.io/blog/python-code-quality/): atricle that explain lint, formatter, security and safe scanning
* [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/): For implement Continous Integration
* [Gitlab CI/CD Coverage and custom badge](https://docs.gitlab.com/ee/ci/pipelines/settings.html): to have coverage badge and multiple ones
