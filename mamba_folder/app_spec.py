from expects import expect, equal
from fastapi.testclient import TestClient
from mamba import description, it, before

from src.main import app

with description('FastApi Server root') as self:
    with before.all:
        self.client = TestClient(app)

    with it('should return 200 and message'):
        response = self.client.get("/")

        expect(response.status_code).to(equal(200))
        expect(response.json()).to(equal({"msg": "Hello World"}))
