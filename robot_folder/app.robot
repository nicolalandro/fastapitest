*** Settings ***
Library           robot_folder.utils

*** Variables ***
${path}             /

*** Test Cases ***

Root get return message and 200
    Fast Api Get   ${path}
    Response Status Code Should Be  200
    Response Message Is             {"msg": "Hello World"}