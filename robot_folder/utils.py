import json

from fastapi.testclient import TestClient

from src.main import app


def _report_failure(msg):
    if msg is None:
        raise AssertionError()
    raise AssertionError(msg)


respose_status_code = -1
response_message = None


def fast_api_get(path):
    global respose_status_code
    global response_message
    client = TestClient(app)
    response = client.get(path)
    respose_status_code = response.status_code
    response_message = response.json()
    return response


def response_status_code_should_be(status_code):
    if not str(respose_status_code) == status_code:
        _report_failure(
            f'Status code is `{respose_status_code}` not `{status_code}`.'
        )


def response_message_is(msg):
    if not response_message == json.loads(msg):
        _report_failure(
            f'Response message is `{response_message}` not `{msg}`.'
        )
